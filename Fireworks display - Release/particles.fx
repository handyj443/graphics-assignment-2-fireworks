//_________________________________________________________________
// CONSTANTS
Texture2D txDiffuse : register(t0);
SamplerState samplerState : register(s0);

cbuffer cbNeverChanges : register(b0)
{
};

cbuffer cbChangesOnResize : register(b1) 
{
	matrix gProj;
};

cbuffer cbChangesEveryFrame : register(b2) 
{
	matrix gWorld;
	matrix gView;
	matrix gWVP;
	matrix gViewProj;
	float4 gCameraPos;
};

cbuffer cbChangesPerObject : register(b3) 
{
};

//_________________________________________________________________
// VERTEX LAYOUTS
struct VS_INPUT
{
	float3 PosL : POSITION;
	float4 Color : COLOR;
	float Size : SIZE;
};

struct GS_INPUT
{
	float3 CenterW : POSITION;
	float4 Color : COLOR;
	float Size : SIZE;
};

struct PS_INPUT
{
	float4 PosH : SV_POSITION;
	float4 Color : COLOR;
	float2 Tex : TEXCOORD0;
};


//_________________________________________________________________
// VERTEX SHADER
GS_INPUT VS(VS_INPUT VSIn)
{
	GS_INPUT VSOut;
	
	// Transform to world space and pass over to the geometry shader.
	VSOut.CenterW = mul(float4(VSIn.PosL, 1), gWorld).xyz;

	// Pass through other values straight to geometry shader.
	VSOut.Color = VSIn.Color;
	VSOut.Size = VSIn.Size;
    
    return VSOut;
}

//_________________________________________________________________
// GEOMETRY SHADER
[maxvertexcount(4)]
void GS(point GS_INPUT GSIn[1], inout TriangleStream<PS_INPUT> triStream)
{
	// Create camera facing quad
	float3 up = float3(0, 1, 0);
	// w: Towards camera
	float3 w = gCameraPos.xyz - GSIn[0].CenterW;
	w = normalize(w);
	// u: Right
	float3 u = cross(up, w);
	u = normalize(u);
	// v: Towards top of quad
	float3 v = cross(w, u);

	float3 halfWidth = 0.5f * GSIn[0].Size * u;
	float3 halfHeight = 0.5f * GSIn[0].Size * v;

	// Compute quad vertex positions in world space
	//      v
	//      ^
	//   0-----1
	//    |   |
	// u<-|   |
	//    |   |
	//   2-----3
	float4 vertices[4];
	vertices[0] = float4(GSIn[0].CenterW + halfWidth + halfHeight, 1);
	vertices[1] = float4(GSIn[0].CenterW - halfWidth + halfHeight, 1);
	vertices[2] = float4(GSIn[0].CenterW + halfWidth - halfHeight, 1);
	vertices[3] = float4(GSIn[0].CenterW - halfWidth - halfHeight, 1);

	float2 gTex[4] =
	{
		float2(0.0f, 0.0f),
		float2(1.0f, 0.0f),
		float2(0.0f, 1.0f),
		float2(1.0f, 1.0f)
	};

	// Transform to screen space and output as triangle strip
	PS_INPUT GSOut;
	[unroll]
	for (int i = 0; i < 4; ++i) {
		GSOut.PosH = mul(vertices[i], gViewProj);
		GSOut.Color = GSIn[0].Color;
		GSOut.Tex = gTex[i];
		triStream.Append(GSOut);
	}
}

//_________________________________________________________________
// PIXEL SHADERS
// Texture sampling pixel shader
float4 PS(PS_INPUT PSIn) : SV_Target
{
	float4 texColor = txDiffuse.Sample(samplerState, PSIn.Tex);
	texColor = texColor * PSIn.Color;
	return texColor;

	//// TESTING: only output color
	////float4 color = PSIn.Color;
	//float4 color = float4(0.5, 0.0, 1.0, .2);
	//return color;
}

// Solid fill pixel shader
float4 PSFill(PS_INPUT PSIn) : SV_Target
{
	float4 white = float4(1, 1, 1, 1);
	//return PSIn.Color;
	return white;
}