//_________________________________________________________________
// CONSTANTS
cbuffer cbNeverChanges : register(b0) {};

cbuffer cbChangesOnResize : register(b1) {
	matrix gProj;
};

cbuffer cbChangesEveryFrame : register(b2) {
	matrix gWorld;
	matrix gView;
	matrix gWVP;
	matrix gViewProj;
	float4 gCameraPos;
};

cbuffer cbChangesPerObject : register(b3) {};

//_________________________________________________________________
// VERTEX LAYOUTS
struct VS_INPUT
{
	float3 PosL : POSITION;
	float4 Color : COLOR;
	float Size : SIZE;
};

struct PS_INPUT
{
	float4 PosH : SV_POSITION;
	float4 Color : COLOR;
};


//_________________________________________________________________
// VERTEX SHADER
PS_INPUT VS(VS_INPUT VSIn) {
	PS_INPUT VSOut;

	// Transform to homogenous coords
	VSOut.PosH = mul(float4(VSIn.PosL, 1), gWVP);

	// Pass through other values
	VSOut.Color = VSIn.Color;

	return VSOut;
}

//_________________________________________________________________
// PIXEL SHADERS
// Texture sampling pixel shader
float4 PS(PS_INPUT PSIn) : SV_Target
{
	//float4 white = float4(1, 1, 1, 1);
	//return white;
	float4 brighten = float4(1.2, 1.2, 1.2, 1);
	float4 color = PSIn.Color * brighten;
	return PSIn.Color;
}