// David Han. Jan 2016.

// Render fireworks with a particle system using a geometry shader 
// to generate camera facing textured quads.
// Read in a fireworks sequence from a file and play it back
// with music.

// DirectX conventions: CW triangle winding, LH coordinate system
// NOTE: simulation steps are currently not frame-rate independent!

// Libraries used:
// DirectXTK for texture loading and text display: 
// https://github.com/Microsoft/DirectXTK/
// SFML for music playback: 
// http://www.sfml-dev.org/

#include <Windows.h>
#include <windowsx.h>
#include <tchar.h>
#include <d3d11.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>
#include <DirectXColors.h>
#include "DDSTextureLoader.h"
#include "WICTextureLoader.h"
#include "SpriteFont.h"
#include "SpriteBatch.h"
#include <dinput.h>
#include <string>
#include <vector>
#include <random>
#include <fstream>
#include "SFML/Audio/Music.hpp"

#include "InputControllers.h"
#include "GameTimer.h"
#include "DefaultStates.h"
#include "Particles.h"
#include "FireworkColors.h"

#include "d3dUtil.h"

using std::string;
using std::wstring;
using std::vector;
using std::ifstream;

using namespace DirectX;

#define MULTISAMPLE
//#define FULLSCREEN

//______________________________________________________________________________
// STRUCTURES

struct VertexPosColSize
{
	XMFLOAT3 Pos;
	XMFLOAT4 Col;
	float Size;
};

struct CBNeverChanges
{
};

struct CBChangesOnResize
{
	XMMATRIX proj;
};

struct CBChangesEveryFrame
{
	XMMATRIX world;
	XMMATRIX view;
	XMMATRIX WVP;
	XMMATRIX viewProj;
	XMVECTOR cameraPos;
};

struct CBChangesPerObject
{
};



//______________________________________________________________________________
// FORWARD DECLARATIONS
void OnResize();

//______________________________________________________________________________
// GLOBALS

// Direct3D
ID3D11Device *gd3dDevice;
ID3D11DeviceContext *gImmediateContext;
IDXGISwapChain *gSwapChain;
ID3D11RenderTargetView *gRenderTargetView;
// NOTE: why is the depth buffer global when the backbuffer isn't?
ID3D11Texture2D *gDepthStencilBuffer;  
ID3D11DepthStencilView *gDepthStencilView;
ID3D11VertexShader *gVSParticles;
ID3D11VertexShader *gVSStatic;
ID3D11GeometryShader *gGSParticles;
ID3D11PixelShader *gPSStatic;
ID3D11PixelShader *gPSParticles;
ID3D11PixelShader *gPSFill;
ID3D11InputLayout *gPosColInputLayout;
ID3D11InputLayout *gPosTexInputLayout;
D3D11_VIEWPORT gScreenViewport;
UINT gMsaa4xQuality = 1; // Assume only one level for now...

ID3D11Buffer *gCBNeverChanges;
ID3D11Buffer *gCBChangesOnResize;
ID3D11Buffer *gCBChangesEveryFrame;
ID3D11Buffer *gCBChangesPerObject;
ID3D11BlendState *gEnableAlphaBS;
ID3D11RasterizerState *gNormalRS;
ID3D11RasterizerState *gWireframeRS;
ID3D11DepthStencilState *gNormalDSS;
ID3D11DepthStencilState *gNoZDSS;

// Geometry and materials
ID3D11Buffer * gStaticVB;
ID3D11Buffer * gStaticIB;

Particles *gParticles;
vector<Firework> gFireworksDisplay;
vector<Firework>::const_iterator gFireworksIter;
ID3D11Buffer *gParticlesVB;
ID3D11ShaderResourceView *gFireworkTextureRV;
ID3D11SamplerState *gSamplerAniso;

// Application control
HWND gMainWnd;
wstring gMainWndCaption;
bool gAppCreated;
bool gAppPaused;
bool gMinimized;
bool gMaximized;
bool gResizing;
#ifdef FULLSCREEN
int gClientWidth = 1920;
int gClientHeight = 1080;
#else
int gClientWidth = 1280;
int gClientHeight = 720;
#endif
GameTimer gTimer;
float gFps;
float gMsPerFrame;

// Input controllers
KeyboardController gKeyboard;
IDirectInput8* gdinput = nullptr;
MouseController* gMouse = nullptr;  // using DirectInput

// Camera
XMMATRIX gWorld;
XMMATRIX gView;
XMMATRIX gProj;
// Angled left view
//float gCamAzimuth = 4.417f;
//float gCamPolar = 1.275f;
//float gRadius = 412.0f;
// Frontal view
 float gCamAzimuth = 4.712f;
 float gCamPolar = 1.76f;
 float gRadius = 428.0f;
XMVECTOR gCameraPos;

// Audio
#ifndef _DEBUG
sf::Music gMusic;
#endif

XMFLOAT4 XMVectorToFloat(const XMVECTORF32 &v) {
	XMFLOAT4 ret;
	XMStoreFloat4(&ret, v);
	return ret;
}

// Text
SpriteFont *gFont;
SpriteBatch *gSpriteBatch;

//_________________________________________________________________
// APPLICATION SPECIFIC FUNCTIONS

inline double CalculateLaunchTime(double secondsPerBeat, int bar, 
								  float beat, float offset) {
	const double BEATS_PER_BAR = 4.0;
	return secondsPerBeat * ((bar - 1) * BEATS_PER_BAR + (beat - 1.0f)) + offset;
}

inline XMFLOAT3 CalculateLaunchVelocity(float angle, float speed) {
	XMFLOAT3 ret;
	std::default_random_engine e;
	std::uniform_real_distribution<float> speedVar(0.80f, 1.20f);
	std::uniform_real_distribution<float> zSpeedVar(-15.0f, 15.0f);

	ret.x = XMScalarSin(angle / 180.0f * XM_PI) * speed * speedVar(e);
	ret.y = XMScalarCos(angle / 180.0f * XM_PI) * speed * speedVar(e);
	ret.z = zSpeedVar(e);
	return ret;
}

void LoadFireworksSequenceFromFile(const string &filename, 
	                           vector<Firework> &gFireworksDisplay) {
	const float OFFSET = 3.454f;  // Time of first beat of song
	ifstream in(filename);
	string prefix;
	double BPM;
	int bar;
	float beat;
	XMFLOAT3 launchPos;
	float angle, speed;
	string color;
	XMFLOAT4 col;
	in >> BPM;
	in >> prefix;
	double secondsPerBeat = 60.0f / BPM;
	while (in) {
		if (prefix[0] != '/') {
			in >> bar >> beat >> launchPos.x >> launchPos.y >> launchPos.z
			   >> angle >> speed >> color;
			Firework f;
			switch (prefix[0]) {
				case 'E':
					f.type = Firework::RISER;
					f.launchTime = CalculateLaunchTime(
						secondsPerBeat, bar, beat, 
						OFFSET - gParticles->RISER_EXPIRY_TIME);
					break;
				case 'H':
					f.type = Firework::HOLLOW_RISER;
					f.launchTime = CalculateLaunchTime(
						secondsPerBeat, bar, beat, 
						OFFSET - gParticles->RISER_EXPIRY_TIME);
					break;
				case 'R':
					f.type = Firework::ROCKET;
					f.launchTime = CalculateLaunchTime(
						secondsPerBeat, bar, beat, OFFSET);
					break;
			}
			f.launchPos = launchPos;
			f.launchVel = CalculateLaunchVelocity(angle, speed);
			f.color = InterpretColor(color);
			gFireworksDisplay.push_back(f);
		}
		// Go to next line
		in.ignore((std::numeric_limits<int>::max)(), '\n');
		in >> prefix;
	}
	in.close();
}

void InitializeApp() {
	gWorld = XMMatrixIdentity();
	gView = XMMatrixIdentity();
	gProj = XMMatrixIdentity();

	// Initialise camera
	//gCamAzimuth = 1.5f * XM_PI;
	//gCamPolar = 0.25f * XM_PI;
	//gRadius = 5;

	// Initialise music
#ifndef _DEBUG
	if (!gMusic.openFromFile("with confidence.ogg")) {
		MessageBox(0, L"Failed to load music.", 0, 0);
	}
#endif

	// Initialise font
	gSpriteBatch = new SpriteBatch(gImmediateContext);
	gFont = new SpriteFont(gd3dDevice, L"arial12.spritefont");

	//_____________________________________________________________
	// Create but do not populate constant buffers
	D3D11_BUFFER_DESC cbdesc = {};
	cbdesc.Usage = D3D11_USAGE_DEFAULT;
	cbdesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	//cbdesc.ByteWidth = sizeof(CBNeverChanges);
	//HR(gd3dDevice->CreateBuffer(&cbdesc, nullptr, &gCBNeverChanges));
	
	cbdesc.ByteWidth = sizeof(CBChangesOnResize);
	HR(gd3dDevice->CreateBuffer(&cbdesc, nullptr, &gCBChangesOnResize));

	cbdesc.ByteWidth = sizeof(CBChangesEveryFrame);
	HR(gd3dDevice->CreateBuffer(&cbdesc, nullptr, &gCBChangesEveryFrame));

	//cbdesc.ByteWidth = sizeof(CBChangesPerObject);
	//HR(gd3dDevice->CreateBuffer(&cbdesc, nullptr, &gCBChangesPerObject));


	//_____________________________________________________________
	// Initialise static geometry
	// Ground plane
	const float GROUND_SIZE = 1000.0f;
	XMFLOAT4 groundColor = { 0.03137f, 0.25098f, 0.06274f, 1.0f };
	vector<VertexPosColSize> vertices = {
		{ XMFLOAT3(-GROUND_SIZE, 0.0f, -GROUND_SIZE), groundColor, 0 },
		{ XMFLOAT3(-GROUND_SIZE, 0.0f,  GROUND_SIZE), groundColor, 0 },
		{ XMFLOAT3( GROUND_SIZE, 0.0f,  GROUND_SIZE), groundColor, 0 },
		{ XMFLOAT3( GROUND_SIZE, 0.0f, -GROUND_SIZE), groundColor, 0 }
	};
	// 16 bit index buffer
	WORD indices[] =
	{
		0,1,2,
		0,2,3
	};

	// Sky box
	const float SKY_BOX_SIZE = 4000.0f;
	// Hacky: re-purpose vertex color as tex coords



	// Create static vertex and index buffer
	D3D11_BUFFER_DESC vbdesc = {};
	ZeroMemory(&vbdesc, sizeof(vbdesc));
	vbdesc.Usage = D3D11_USAGE_DEFAULT;
	vbdesc.ByteWidth = sizeof(VertexPosColSize) * 4;
	vbdesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vbdesc.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = &vertices[0];
	HR(gd3dDevice->CreateBuffer(&vbdesc, &InitData, &gStaticVB));

	vbdesc.ByteWidth = sizeof(WORD) * 6;
	vbdesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	InitData.pSysMem = indices;
	HR(gd3dDevice->CreateBuffer(&vbdesc, &InitData, &gStaticIB));


	//_____________________________________________________________
	// Initialise particle system
	const float INITIAL_SIZE = 0.4f;
	const int MAX_PARTICLES = 3000000;
	gParticles = new Particles(MAX_PARTICLES);
	vbdesc = {};
	// Dynamic vertex buffer for particles
	vbdesc.Usage = D3D11_USAGE_DYNAMIC;
	vbdesc.ByteWidth = MAX_PARTICLES * sizeof(VertexPosColSize);
	vbdesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vbdesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	HR(gd3dDevice->CreateBuffer(&vbdesc, nullptr, &gParticlesVB));

	// Load Fireworks Display from file
	//LoadFireworksSequenceFromFile("testing.txt", gFireworksDisplay);
	LoadFireworksSequenceFromFile("with confidence sequence.txt", gFireworksDisplay);

	// Ensure vector is sorted in chronological order
	sort(gFireworksDisplay.begin(), gFireworksDisplay.end(),
		 [](Firework &a, Firework &b) { return a.launchTime < b.launchTime; });

	// Set fireworks iterator to the beginning ready for first Update()
	gFireworksIter = gFireworksDisplay.begin();

	//_____________________________________________________________
	// Load textures
	HRESULT hr = CreateWICTextureFromFile(gd3dDevice, _T("fireworksParticle.png"), 
										  nullptr, &gFireworkTextureRV);
	if (FAILED(hr)) {
		MessageBox(0, L"CreateDDSTextureFromFile failed.", 0, 0);
	}

	// Create Sampler State
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_ANISOTROPIC;
	sampDesc.MaxAnisotropy = 8;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
	hr = gd3dDevice->CreateSamplerState(&sampDesc, &gSamplerAniso);
	if (FAILED(hr)) {
		MessageBox(0, L"CreateSamplerState failed.", 0, 0);
	}

	//_____________________________________________________________
	// Create device states
	D3D11_RASTERIZER_DESC normalRastDesc = GetDefaultRasterizerDesc();
	normalRastDesc.CullMode = D3D11_CULL_BACK;
	normalRastDesc.MultisampleEnable = true;
	HR(gd3dDevice->CreateRasterizerState(&normalRastDesc, &gNormalRS));

	D3D11_RASTERIZER_DESC wireframeDesc = GetDefaultRasterizerDesc();
	wireframeDesc.FillMode = D3D11_FILL_WIREFRAME;
	wireframeDesc.CullMode = D3D11_CULL_NONE;
	wireframeDesc.MultisampleEnable = true;
	HR(gd3dDevice->CreateRasterizerState(&wireframeDesc, &gWireframeRS));

	D3D11_DEPTH_STENCIL_DESC normalDepthDesc = GetDefaultDepthStencilDesc();
	HR(gd3dDevice->CreateDepthStencilState(&normalDepthDesc, &gNormalDSS));

	D3D11_DEPTH_STENCIL_DESC noZDesc = GetDefaultDepthStencilDesc();
	noZDesc.DepthEnable = false;
	HR(gd3dDevice->CreateDepthStencilState(&noZDesc, &gNoZDSS));

	D3D11_BLEND_DESC enableAlphaDesc = GetDefaultBlendDesc();
	enableAlphaDesc.AlphaToCoverageEnable = true;
	enableAlphaDesc.RenderTarget[0].BlendEnable = true;
	enableAlphaDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	enableAlphaDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	enableAlphaDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	enableAlphaDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	enableAlphaDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	enableAlphaDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	HR(gd3dDevice->CreateBlendState(&enableAlphaDesc, &gEnableAlphaBS));
	
	// Set constant buffer variables that never change
	//CBNeverChanges cb;
	//gImmediateContext->UpdateSubresource(gCBNeverChanges, 0, nullptr, &cb, 0, 0);
#ifndef _DEBUG
	gMusic.play();
#endif
}

void ShutdownApp() {
	delete gFont;
	delete gSpriteBatch;
	delete gParticles;
	ReleaseCOM(gStaticVB);
	ReleaseCOM(gStaticIB);
	ReleaseCOM(gParticlesVB);
	ReleaseCOM(gWireframeRS);
	ReleaseCOM(gNormalRS);
	ReleaseCOM(gEnableAlphaBS);
	ReleaseCOM(gNormalDSS);
	ReleaseCOM(gNoZDSS);
	ReleaseCOM(gPosColInputLayout);
	ReleaseCOM(gPosTexInputLayout);
	ReleaseCOM(gFireworkTextureRV);
	ReleaseCOM(gSamplerAniso);
}

//_________________________________________________________________
// UPDATE
void UpdateScene(float dt) {
	gMouse->poll();

	 //Move global camera
	if (gMouse->rightdown()) {
		const float MOUSE_DOLLY_SENSITIVITY = 0.007f;
		// Update the camera radius based on input.
		float scaling = 1 + gMouse->dx() * MOUSE_DOLLY_SENSITIVITY;
		gRadius *= scaling;
	
		// Restrict the radius.
		gRadius = MathHelper::Clamp(gRadius, 1.0f, 1000.0f);
	}
	else if (gMouse->leftdown()) {
		const float MOUSE_SPIN_SENSITIVITY = 0.007f;
		// Update angles based on input to orbit camera around box.
		gCamAzimuth -= gMouse->dx() * MOUSE_SPIN_SENSITIVITY;
		gCamPolar -= gMouse->dy() * MOUSE_SPIN_SENSITIVITY;
	
		// Restrict the angle mPhi.
		gCamPolar = MathHelper::Clamp(gCamPolar, 0.1f, XM_PI-0.1f);
	}

	// Convert Spherical to Cartesian coordinates.
	float x = gRadius*sinf(gCamPolar)*cosf(gCamAzimuth);
	float z = gRadius*sinf(gCamPolar)*sinf(gCamAzimuth);
	float y = gRadius*cosf(gCamPolar);

	// Build the view matrix.
	XMVECTOR target = XMVectorSet(0, 170, 0, 1);
	gCameraPos = XMVectorSet(x, y, z, 1.0f) + target;
	XMVECTOR up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);

	gView = XMMatrixLookAtLH(gCameraPos, target, up);

	// Emit particles
	while (gFireworksIter != gFireworksDisplay.end()) {
		if (gFireworksIter->launchTime > gTimer.TotalTime()) {
			break;
		}
		gParticles->AddFirework(*gFireworksIter);
		++gFireworksIter;
	}

	// Update particles
	XMFLOAT3 gravity = { 0, -9.8f, 0 };
	gParticles->Update(dt, gravity);

	// Copy particles data to vertex buffer
	D3D11_MAPPED_SUBRESOURCE mappedData;
	HR(gImmediateContext->Map(
		gParticlesVB, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedData));

	VertexPosColSize *v = reinterpret_cast<VertexPosColSize*>(mappedData.pData);
	for (size_t i = 0; i < gParticles->numParticles; ++i) {
		size_t ind = (i + gParticles->oldestValidParticle) % gParticles->MAX_PARTICLES;
		v[i].Pos = gParticles->pos[ind];
		v[i].Col = gParticles->color[ind];
		v[i].Size = gParticles->size[ind];
	}
	gImmediateContext->Unmap(gParticlesVB, 0);
}

//_________________________________________________________________
// RENDER
void DrawScene() {
	XMVECTORF32 NightSky = { 0.07058f, 0.07450f, 0.21568f, 1.000000000f };
	gImmediateContext->ClearRenderTargetView(
		gRenderTargetView, NightSky);
	gImmediateContext->ClearDepthStencilView(
		gDepthStencilView, D3D11_CLEAR_DEPTH|D3D11_CLEAR_STENCIL, 1.0f, 0);

	// Update constant buffer variables that change once per frame
	XMMATRIX viewProj = gView * gProj;
	XMMATRIX WVP = gWorld * viewProj;
	CBChangesEveryFrame cb;
	cb.view = XMMatrixTranspose(gView);
	cb.world = XMMatrixTranspose(gWorld);
	cb.WVP = XMMatrixTranspose(WVP);
	cb.viewProj = XMMatrixTranspose(viewProj);
	cb.cameraPos = gCameraPos;
	gImmediateContext->UpdateSubresource(gCBChangesEveryFrame, 0, nullptr, &cb, 0, 0);


	// Set common states
	gImmediateContext->RSSetState(gNormalRS);
	gImmediateContext->OMSetDepthStencilState(gNormalDSS, 0);
	gImmediateContext->OMSetBlendState(gEnableAlphaBS, nullptr, 0xffffffff);
	gImmediateContext->IASetInputLayout(gPosTexInputLayout);
	gImmediateContext->VSSetConstantBuffers(2, 1, &gCBChangesEveryFrame);
	gImmediateContext->GSSetConstantBuffers(1, 1, &gCBChangesOnResize);
	gImmediateContext->GSSetConstantBuffers(2, 1, &gCBChangesEveryFrame);
	gImmediateContext->PSSetSamplers(0, 1, &gSamplerAniso);
	
	// Render static geometry
	UINT stride = sizeof(VertexPosColSize);
	UINT offset = 0;
	gImmediateContext->IASetVertexBuffers(0, 1, &gStaticVB, &stride, &offset);
	gImmediateContext->IASetIndexBuffer(gStaticIB, DXGI_FORMAT_R16_UINT, 0);
	gImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	gImmediateContext->VSSetShader(gVSStatic, nullptr, 0);
	gImmediateContext->GSSetShader(nullptr, nullptr, 0);
	gImmediateContext->PSSetShader(gPSStatic, nullptr, 0);
	gImmediateContext->DrawIndexed(6, 0, 0);

	// Render text
	gSpriteBatch->Begin();
	wchar_t textBuffer[256];
	swprintf_s(textBuffer, 
			   L"Frames Per Second = %.2f\n"
			   L"Milliseconds Per Frame = %.4f\n"
			   L"Number of particles = %d\n"
			   L"Oldest valid particle = %d\n"
			   L"New particle index = %d\n\n"
			   L"Left click: Rotate camera\n"
			   L"Right click: Zoom camera\n"
			   L"Alt-Enter: Full screen\n",
			   gFps, gMsPerFrame, gParticles->numParticles, 
			   gParticles->oldestValidParticle, gParticles->newParticleIndex);
	gFont->DrawString(gSpriteBatch, textBuffer, XMFLOAT2(10, 10), Colors::White);
	gSpriteBatch->End();

	// Reset states
	gImmediateContext->RSSetState(gNormalRS);
	gImmediateContext->OMSetDepthStencilState(gNormalDSS, 0);
	gImmediateContext->OMSetBlendState(gEnableAlphaBS, nullptr, 0xffffffff);
	gImmediateContext->IASetInputLayout(gPosTexInputLayout);
	gImmediateContext->VSSetConstantBuffers(2, 1, &gCBChangesEveryFrame);
	gImmediateContext->GSSetConstantBuffers(1, 1, &gCBChangesOnResize);
	gImmediateContext->GSSetConstantBuffers(2, 1, &gCBChangesEveryFrame);
	gImmediateContext->PSSetSamplers(0, 1, &gSamplerAniso);

	// Render particles
	gImmediateContext->IASetVertexBuffers(0, 1, &gParticlesVB, &stride, &offset);
	gImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
	gImmediateContext->VSSetShader(gVSParticles, nullptr, 0);
	gImmediateContext->GSSetShader(gGSParticles, nullptr, 0);
	gImmediateContext->PSSetShader(gPSParticles, nullptr, 0);
	gImmediateContext->PSSetShaderResources(0, 1, &gFireworkTextureRV);
	gImmediateContext->Draw(gParticles->numParticles, 0);


	// Present back buffer
	HR(gSwapChain->Present(1, 0)); // vsync
}

//_________________________________________________________________
// GENERAL FUNCTIONS

void OnResize() {
	// Release the old views, as they hold references to the buffers we
	// will be destroying.  Also release the old depth/stencil buffer.
	ReleaseCOM(gRenderTargetView);
	ReleaseCOM(gDepthStencilBuffer);
	ReleaseCOM(gDepthStencilView);

	// Resize the swap chain and (re)create the render target view.
	HR(gSwapChain->ResizeBuffers(1, gClientWidth, gClientHeight, DXGI_FORMAT_R8G8B8A8_UNORM, 0));
	ID3D11Texture2D* renderTargetBuffer;
	HR(gSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D),
							 (void**)(&renderTargetBuffer)));
	HR(gd3dDevice->CreateRenderTargetView(renderTargetBuffer, 0, &gRenderTargetView));
	ReleaseCOM(renderTargetBuffer);

	// (Re)create the depth/stencil buffer and view.
	D3D11_TEXTURE2D_DESC depthStencilDesc;
	ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));
	depthStencilDesc.Width = gClientWidth;
	depthStencilDesc.Height = gClientHeight;
	depthStencilDesc.MipLevels = 1;
	depthStencilDesc.ArraySize = 1;
	depthStencilDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
#ifdef MULTISAMPLE
	depthStencilDesc.SampleDesc.Count = 4;
	depthStencilDesc.SampleDesc.Quality = gMsaa4xQuality-1;
#else
	depthStencilDesc.SampleDesc.Count = 1;
	depthStencilDesc.SampleDesc.Quality = 0;
#endif
	depthStencilDesc.Usage = D3D11_USAGE_DEFAULT;
	depthStencilDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthStencilDesc.CPUAccessFlags = 0;
	depthStencilDesc.MiscFlags = 0;
	HR(gd3dDevice->CreateTexture2D(&depthStencilDesc, 0, &gDepthStencilBuffer));

	// Create the depth stencil view
	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory(&descDSV, sizeof(descDSV));
	descDSV.Format = depthStencilDesc.Format;
#ifdef MULTISAMPLE
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
#else
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
#endif
	descDSV.Texture2D.MipSlice = 0;
	HR(gd3dDevice->CreateDepthStencilView(gDepthStencilBuffer, &descDSV, &gDepthStencilView));

	// Bind the render target view and depth/stencil view to the pipeline.
	gImmediateContext->OMSetRenderTargets(1, &gRenderTargetView, gDepthStencilView);

	// Set the viewport transform.
	gScreenViewport.TopLeftX = 0;
	gScreenViewport.TopLeftY = 0;
	gScreenViewport.Width = static_cast<float>(gClientWidth);
	gScreenViewport.Height = static_cast<float>(gClientHeight);
	gScreenViewport.MinDepth = 0.0f;
	gScreenViewport.MaxDepth = 1.0f;
	gImmediateContext->RSSetViewports(1, &gScreenViewport);

	// The window resized, so update the aspect ratio and recompute the 
	// projection matrix.
	gProj = XMMatrixPerspectiveFovLH(
		0.3f * XM_PI, (float)gClientWidth / gClientHeight, 2.0f, 8000.0f);

	// Update constant buffer variables that change on resize
	CBChangesOnResize cb;
	cb.proj = XMMatrixTranspose(gProj);
	gImmediateContext->UpdateSubresource(gCBChangesOnResize, 0, nullptr, &cb, 0, 0);
}

// Computes the average frames per second, and the average time to render 
// one frame.  These stats are appended to the window caption bar.
void CalculateFrameStats() {
	static int frameCnt = 0;
	static float timeElapsed = 0.0f;
	
	frameCnt++;
	// Compute averages over one second period.
	if ((gTimer.TotalTime() - timeElapsed) >= 1.0f) {
		gFps = (float)frameCnt; // fps = frameCnt / 1
		gMsPerFrame = 1000.0f / gFps;
		std::wostringstream outs;
		outs.precision(6);
		outs << gMainWndCaption << L"    "
			<< L"FPS: " << gFps << L"    "
			<< L"Frame Time: " << gMsPerFrame << L" (ms)   "
			<< L"No. Particles: " << gParticles->numParticles;
		SetWindowText(gMainWnd, outs.str().c_str());
		// Reset for next average.
		frameCnt = 0;
		timeElapsed += 1.0f;
	}
}

HRESULT CompileShaderFromFile(WCHAR* szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut) {
	HRESULT hr = S_OK;

	DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#ifdef _DEBUG
	// Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
	// Setting this flag improves the shader debugging experience, but still allows 
	// the shaders to be optimized and to run exactly the way they will run in 
	// the release configuration of this program.
	dwShaderFlags |= D3DCOMPILE_DEBUG;

	// Disable optimizations to further improve shader debugging
	dwShaderFlags |= D3DCOMPILE_SKIP_OPTIMIZATION;
#endif

	ID3DBlob* pErrorBlob = nullptr;
	hr = D3DCompileFromFile(szFileName, nullptr, nullptr, szEntryPoint, szShaderModel,
							dwShaderFlags, 0, ppBlobOut, &pErrorBlob);
	if (FAILED(hr)) {
		if (pErrorBlob) {
			OutputDebugStringA(
				reinterpret_cast<const char*>(pErrorBlob->GetBufferPointer()));
			pErrorBlob->Release();
		}
		return hr;
	}
	if (pErrorBlob) pErrorBlob->Release();

	return S_OK;
}

//______________________________________________________________________________
// WINDOWS CALLBACK
LRESULT MsgProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
	switch (msg) {
		// WM_ACTIVATE is sent when the window is activated or deactivated.  
		// We pause the game when the window is deactivated and unpause it 
		// when it becomes active.  
		case WM_ACTIVATE:
			if (LOWORD(wParam) == WA_INACTIVE) {
				gAppPaused = true;
				gTimer.Stop();
#ifndef _DEBUG
				gMusic.pause();
#endif
			}
			else {
				gAppPaused = false;
				gTimer.Start();
#ifndef _DEBUG
				gMusic.play();
#endif
			}
			return 0;

		// WM_SIZE is sent when the user resizes the window.  
		case WM_SIZE:
			// Save the new client area dimensions.
			gClientWidth = LOWORD(lParam);
			gClientHeight = HIWORD(lParam);
			if (gd3dDevice) {
				if (wParam == SIZE_MINIMIZED) {
					gAppPaused = true;
					gMinimized = true;
					gMaximized = false;
					gTimer.Stop();
#ifndef _DEBUG
					gMusic.pause();
#endif
				}
				else if (wParam == SIZE_MAXIMIZED) {
					gAppPaused = false;
					gMinimized = false;
					gMaximized = true;
					OnResize();
					gTimer.Start();
#ifndef _DEBUG
					gMusic.play();
#endif
				}
				else if (wParam == SIZE_RESTORED) {
					// Restoring from minimized state?
					if (gMinimized) {
						gAppPaused = false;
						gMinimized = false;
						OnResize();
						gTimer.Start();
#ifndef _DEBUG
						gMusic.play();
#endif
					}
					// Restoring from maximized state?
					else if (gMaximized) {
						gAppPaused = false;
						gMaximized = false;
						OnResize();
						gTimer.Start();
#ifndef _DEBUG
						gMusic.play();
#endif
					}
					else if (gResizing) {
					// We do not resize while the user is resizing. Instead, 
					// we resize after the user releases the resize bars, 
					// which sends a WM_EXITSIZEMOVE message.
					}
					else {
					// API call such as SetWindowPos or 
					// gSwapChain->SetFullscreenState.
						OnResize();
					}
				}
			}
			return 0;

		// WM_ENTERSIZEMOVE is sent when the user grabs the resize bars.
		case WM_ENTERSIZEMOVE:
			gAppPaused = true;
			gResizing = true;
			gTimer.Stop();
#ifndef _DEBUG
			gMusic.pause();
#endif
			return 0;

		// WM_EXITSIZEMOVE is sent when the user releases the resize bars.
		// Here we reset everything based on the new window dimensions.
		case WM_EXITSIZEMOVE:
			gAppPaused = false;
			gResizing = false;
			gTimer.Start();
#ifndef _DEBUG
			gMusic.play();
#endif
			OnResize();
			return 0;

		// WM_DESTROY is sent when the window is being destroyed.
		case WM_DESTROY:
			PostQuitMessage(0);
			return 0;

		// The WM_MENUCHAR message is sent when a menu is active and the user
		// presses a key that does not correspond to any mnemonic or 
		// accelerator key. 
		case WM_MENUCHAR:
			// Don't beep when we alt-enter.
			return MAKELRESULT(0, MNC_CLOSE);

		// Catch this message so to prevent the window from becoming too small.
		case WM_GETMINMAXINFO:
			((MINMAXINFO*)lParam)->ptMinTrackSize.x = 200;
			((MINMAXINFO*)lParam)->ptMinTrackSize.y = 200;
			return 0;
	}

	return DefWindowProc(hwnd, msg, wParam, lParam);
}

LRESULT CALLBACK
WinProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
	// Don't start processing messages until the application has been created.
	if (gAppCreated)
		return MsgProc(hwnd, msg, wParam, lParam);
	else
		return DefWindowProc(hwnd, msg, wParam, lParam);
}

//______________________________________________________________________________
// WINMAIN
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
				   PSTR cmdLine, int showCmd) {
	// Enable run-time memory check for debug builds.
#if defined(DEBUG) | defined(_DEBUG)
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	//_____________________________________________________________
	// 1. Initiate window
	WNDCLASS wc;
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WinProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon(0, IDI_APPLICATION);
	wc.hCursor = LoadCursor(0, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(NULL_BRUSH);
	wc.lpszMenuName = 0;
	wc.lpszClassName = _T("D3DWndClassName");

	if (!RegisterClass(&wc)) {
		MessageBox(0, L"RegisterClass Failed.", 0, 0);
		return false;
	}

	// Compute window rectangle dimensions based on client area dimensions.
	RECT R = { 0, 0, gClientWidth, gClientHeight };
	AdjustWindowRect(&R, WS_OVERLAPPEDWINDOW, false);
	int width = R.right - R.left;
	int height = R.bottom - R.top;

	gMainWndCaption = _T("Shaders Are Magic");
	gMainWnd = CreateWindow(_T("D3DWndClassName"), gMainWndCaption.c_str(),
							WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT,
							width, height, 0, 0, hInstance, 0);
	if (!gMainWnd) {
		MessageBox(0, _T("CreateWindow Failed."), 0, 0);
		return false;
	}

	ShowWindow(gMainWnd, SW_SHOW);
	UpdateWindow(gMainWnd);

	//_____________________________________________________________
	// 2. Initiate Direct3D
	// Create device
	UINT createDeviceFlags = 0;
#if defined(DEBUG) || defined(_DEBUG)  
	//createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif
	D3D_FEATURE_LEVEL featureLevels[] = 
	{
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0
	};
	UINT numFeatureLevels = ARRAYSIZE(featureLevels);
	D3D_FEATURE_LEVEL featureLevel;
	D3D_DRIVER_TYPE driverType = D3D_DRIVER_TYPE_HARDWARE;

	HRESULT hr = D3D11CreateDevice(
		nullptr, driverType, nullptr, createDeviceFlags, featureLevels, numFeatureLevels,
		D3D11_SDK_VERSION, &gd3dDevice, &featureLevel, &gImmediateContext);
		if (FAILED(hr)) {
		MessageBox(0, L"D3D11CreateDevice Failed.", 0, 0);
		return false;
	}

	// Obtain DXGI factory from device
	IDXGIFactory1* dxgiFactory = nullptr;
	{
		IDXGIDevice* dxgiDevice = nullptr;
		hr = gd3dDevice->QueryInterface(__uuidof(IDXGIDevice), 
										reinterpret_cast<void**>(&dxgiDevice));
		if (SUCCEEDED(hr)) {
			IDXGIAdapter* adapter = nullptr;
			hr = dxgiDevice->GetAdapter(&adapter);
			if (SUCCEEDED(hr)) {
				hr = adapter->GetParent(__uuidof(IDXGIFactory1), 
										reinterpret_cast<void**>(&dxgiFactory));
				adapter->Release();
			}
			dxgiDevice->Release();
		}
	}
	if (FAILED(hr)) {
		MessageBox(0, L"Failed to obtain DXGI factory from device.", 0, 0);
		return false;
	}

	// Create swap chain from the DXGI factory
	DXGI_SWAP_CHAIN_DESC sd;
	sd.BufferDesc.Width = gClientWidth;
	sd.BufferDesc.Height = gClientHeight;
	sd.BufferDesc.RefreshRate.Numerator = 60;
	sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	sd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	sd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
#ifdef MULTISAMPLE
	sd.SampleDesc.Count = 4;
	sd.SampleDesc.Quality = gMsaa4xQuality-1;
#else
	sd.SampleDesc.Count = 1;
	sd.SampleDesc.Quality = 0;
#endif
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.BufferCount = 1;
	sd.OutputWindow = gMainWnd;
#ifdef FULLSCREEN
	sd.Windowed = false;
#else
	sd.Windowed = true;
#endif;
	sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	sd.Flags = 0;
	hr = dxgiFactory->CreateSwapChain(gd3dDevice, &sd, &gSwapChain);
	// Uncomment to block ALT-ENTER shortcut
	//HR(dxgiFactory->MakeWindowAssociation(gMainWnd, DXGI_MWA_NO_ALT_ENTER));
	dxgiFactory->Release();
	if (FAILED(hr)) {
		MessageBox(0, L"Failed to create swap chain.", 0, 0);
		return false;
	}

	// Create the vertex shaders
	// Particles
	ID3DBlob *pVSBlob = nullptr;
	hr = CompileShaderFromFile(L"particles.fx", "VS", "vs_4_0", &pVSBlob);
	if (FAILED(hr)) {
		MessageBox(nullptr, L"The FX file cannot be compiled.", L"Error", MB_OK);
		return false;
	}
	hr = gd3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), 
										pVSBlob->GetBufferSize(), nullptr, 
										&gVSParticles);
	if (FAILED(hr)) {
		MessageBox(nullptr, L"Cannot create vertex shader.", L"Error", MB_OK);
		pVSBlob->Release();
		return false;
	}
	// Static
	hr = CompileShaderFromFile(L"static.fx", "VS", "vs_4_0", &pVSBlob);
	if (FAILED(hr)) {
		MessageBox(nullptr, L"The FX file cannot be compiled.", L"Error", MB_OK);
		return false;
	}
	hr = gd3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(),
										pVSBlob->GetBufferSize(), nullptr, 
										&gVSStatic);
	if (FAILED(hr)) {
		MessageBox(nullptr, L"Cannot create vertex shader.", L"Error", MB_OK);
		pVSBlob->Release();
		return false;
	}

	// Create the input layout
	D3D11_INPUT_ELEMENT_DESC vertexPosColSizeDesc[] = {
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "SIZE", 0, DXGI_FORMAT_R32_FLOAT, 0, 28, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	hr = gd3dDevice->CreateInputLayout(
		vertexPosColSizeDesc, ARRAYSIZE(vertexPosColSizeDesc),
		pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), 
		&gPosTexInputLayout);
	pVSBlob->Release();
	if (FAILED(hr)) {
		MessageBox(nullptr, L"Cannot create input layouts.", L"Error", MB_OK);
		return false;
	}

	// Create the geometry shader
	ID3DBlob *pGSBlob = nullptr;
	hr = CompileShaderFromFile(L"particles.fx", "GS", "gs_4_0", &pGSBlob);
	if (FAILED(hr)) {
		MessageBox(nullptr, L"The FX file cannot be compiled.", L"Error", MB_OK);
		return false;
	}
	hr = gd3dDevice->CreateGeometryShader(pGSBlob->GetBufferPointer(),
										  pGSBlob->GetBufferSize(), nullptr, &gGSParticles);
	pGSBlob->Release();
	if (FAILED(hr)) {
		MessageBox(nullptr, L"Cannot create geometry shader.", L"Error", MB_OK);
		return false;
	}

	// Create the pixel shaders
	// Particles
	ID3DBlob *pPSBlob = nullptr;
	hr = CompileShaderFromFile(L"particles.fx", "PS", "ps_4_0", &pPSBlob);
	if (FAILED(hr)) {
		MessageBox(nullptr, L"The FX file cannot be compiled.", L"Error", MB_OK);
		pPSBlob->Release();
		return false;
	}
	hr = gd3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), 
									   pPSBlob->GetBufferSize(), nullptr, 
									   &gPSParticles);
	if (FAILED(hr)) {
		MessageBox(nullptr, L"Cannot create pixel shader.", L"Error", MB_OK);
		pPSBlob->Release();
		return false;
	}
	hr = CompileShaderFromFile(L"particles.fx", "PSFill", "ps_4_0", &pPSBlob);
	if (FAILED(hr)) {
		MessageBox(nullptr, L"The FX file cannot be compiled.", L"Error", MB_OK);
		pPSBlob->Release();
		return false;
	}
	hr = gd3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(),
									   pPSBlob->GetBufferSize(), nullptr,
									   &gPSFill);
	if (FAILED(hr)) {
		MessageBox(nullptr, L"Cannot create pixel shader.", L"Error", MB_OK);
		pPSBlob->Release();
		return false;
	}
	// Static
	hr = CompileShaderFromFile(L"static.fx", "PS", "ps_4_0", &pPSBlob);
	if (FAILED(hr)) {
		MessageBox(nullptr, L"The FX file cannot be compiled.", L"Error", MB_OK);
		pPSBlob->Release();
		return false;
	}
	hr = gd3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(),
									   pPSBlob->GetBufferSize(), nullptr,
									   &gPSStatic);
	if (FAILED(hr)) {
		MessageBox(nullptr, L"Cannot create pixel shader.", L"Error", MB_OK);
		pPSBlob->Release();
		return false;
	}

	pPSBlob->Release();

	//_____________________________________________________________
	// 3. Initialize DirectInput
	HR(DirectInput8Create(hInstance, DIRECTINPUT_VERSION,
						  IID_IDirectInput8, (void**)&gdinput, 0));
	MouseController mouseController(
		gdinput, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND);
	gMouse = &mouseController;  // Set a global pointer to the mouse

	//_____________________________________________________________
	// 4. Initialize Application
	InitializeApp();
	gAppCreated = true;

	// The remaining steps that need to be carried out for d3d creation
	// also need to be executed every time the window is resized.  So
	// just call the OnResize method here to avoid code duplication.
	OnResize();

	// Initialisation Complete!
	//*************************************************************
	//_____________________________________________________________
	// Run
	MSG msg = {};
	gTimer.Reset();

	while (msg.message != WM_QUIT) {
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		// Otherwise, do animation/game stuff.
		else {
			gTimer.Tick();

			if (!gAppPaused) {
				CalculateFrameStats();
				UpdateScene(gTimer.DeltaTime());
				DrawScene();
			}
			else {
				Sleep(100);
			}
		}
	}

	// Finished running!
	//****************************************************************
	//________________________________________________________________
	// Begin shutdown procedures
	ShutdownApp();
	ReleaseCOM(gPSParticles);
	ReleaseCOM(gPSStatic);
	ReleaseCOM(gVSParticles);
	ReleaseCOM(gVSStatic);
	ReleaseCOM(gGSParticles);
	ReleaseCOM(gCBNeverChanges);
	ReleaseCOM(gCBChangesOnResize);
	ReleaseCOM(gCBChangesEveryFrame);
	ReleaseCOM(gCBChangesPerObject);
	ReleaseCOM(gRenderTargetView);
	ReleaseCOM(gDepthStencilView);
	ReleaseCOM(gSwapChain);
	ReleaseCOM(gDepthStencilBuffer);
	// Restore all default settings.
	if (gImmediateContext)
		gImmediateContext->ClearState();
	ReleaseCOM(gImmediateContext);
	ReleaseCOM(gd3dDevice);

	return (int)msg.wParam;
}