#pragma once
#include <vector>
#include <random>
#include <d3d11.h>
#include <DirectXMath.h>

struct Firework
{
	enum Type { RISER, HOLLOW_RISER, ROCKET };

	Type type;
	DirectX::XMFLOAT3 launchPos;
	double launchTime;
	DirectX::XMFLOAT3 launchVel;
	DirectX::XMFLOAT4 color;
};

// A particle system designed for fireworks
struct Particles
{
	enum Type { RISER, HOLLOW_RISER, ROCKET, EXPLOSION, TRAIL };

	Particles(size_t n);

	void AddSingleParticle(const DirectX::XMFLOAT3 &pos, const DirectX::XMFLOAT3 &vel,
						   const DirectX::XMFLOAT4 &color, const float size, 
						   Type t);

	// General function to add a firework
	void AddFirework(const Firework &f);

	void AddExplosion(const DirectX::XMFLOAT3 &initialPos, 
					  const float initialSpeed, 
					  const DirectX::XMFLOAT4 &initialColor, const float initialSize,
					  const float speedDeviation, const unsigned int numParticles);

	void Update(const float dt, const DirectX::XMFLOAT3 &acc);

	void Clean();

	std::vector<DirectX::XMFLOAT3> pos;
	std::vector<DirectX::XMFLOAT3> vel;
	std::vector<DirectX::XMFLOAT4> color;
	std::vector<float> size;  // size of quad
	std::vector<float> life;  // time of flight
	std::vector<float> lifetime;  // lifetime before expiry
	std::vector<Type> type;
	std::vector<bool> valid;  // false = mark particle for deletion
	size_t numParticles;
	size_t oldestValidParticle;
	size_t newParticleIndex;
	const size_t MAX_PARTICLES;  // maximum simultaneous particles supported
	const float RISER_EXPIRY_TIME = 2.5f;

private:
	// Indices to maintain circular buffer
	const float ROCKET_EXPIRY_TIME = 2.0f;
	const float EXPLOSION_EXPIRY_TIME = 2.5f;
	const float TRAIL_EXPIRY_TIME = 0.5f;
	const float EXPLOSION_SPEED = 50.0f;
	const float HOLLOW_EXPLOSION_SPEED = 60.0f;
	const float EXPLOSION_QUAD_SIZE = 1.5f;
	const float TRAIL_QUAD_SIZE = 1.0f;
	const float FADE_DURATION = 0.3f;
	static const unsigned int EXPLOSION_NUM_PARTICLES = 360;

	const float dragCoeff = 0.5;  // halve speed every second

	std::default_random_engine e;
};
