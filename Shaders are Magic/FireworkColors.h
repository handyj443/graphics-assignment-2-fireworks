#pragma once
#include <string>
#include <DirectXMath.h>

const DirectX::XMFLOAT4 RED = {0.854901961f, 0.274509804f, 0.266666667f, 1.0f };
const DirectX::XMFLOAT4 YELLOW = {0.901960784f, 0.901960784f, 0.223529412f, 1.0f };
const DirectX::XMFLOAT4 ORANGE = {0.901960784f, 0.588235294f, 0.305882353f, 1.0f };
const DirectX::XMFLOAT4 GREEN = {0.823529412f, 0.890196078f, 0.490196078f, 1.0f };
const DirectX::XMFLOAT4 BLUE = {0.231372549f, 0.345098039f, 0.721568627f, 1.0f };
const DirectX::XMFLOAT4 VIOLET = {0.901960784f, 0.662745098f, 0.835294118f, 1.0f };
const DirectX::XMFLOAT4 WHITE = {0.988235294f, 1.0f, 1.0f, 1.0f };

DirectX::XMFLOAT4 InterpretColor(std::string col) {
	if (col == "RED") {
		return RED;
	}
	if (col == "YELLOW") {
		return YELLOW;
	}
	if (col == "ORANGE") {
		return ORANGE;
	}
	if (col == "GREEN") {
		return GREEN;
	}
	if (col == "BLUE") {
		return BLUE;
	}
	if (col == "VIOLET") {
		return VIOLET;
	}
	if (col == "WHITE") {
		return WHITE;
	}
	return WHITE;  // default
}