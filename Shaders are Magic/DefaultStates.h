#pragma once
#include <d3d11.h>
#include <climits>

D3D11_DEPTH_STENCIL_DESC GetDefaultDepthStencilDesc() {
	D3D11_DEPTH_STENCIL_DESC ret = {};
	ret.DepthEnable = true;
	ret.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	ret.DepthFunc = D3D11_COMPARISON_LESS;
	ret.StencilEnable = false;
	ret.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;
	ret.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;
	ret.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	ret.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
	ret.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	ret.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	ret.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	ret.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
	ret.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	ret.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	return ret;
}

D3D11_RASTERIZER_DESC GetDefaultRasterizerDesc() {
	D3D11_RASTERIZER_DESC ret = {};
	ret.FillMode = D3D11_FILL_SOLID;
	ret.CullMode = D3D11_CULL_BACK;
	ret.FrontCounterClockwise = false;
	ret.DepthBias = 0;
	ret.SlopeScaledDepthBias = 0.0f;
	ret.DepthBiasClamp = 0.0f;
	ret.DepthClipEnable = true;
	ret.ScissorEnable = false;
	ret.MultisampleEnable = false;
	ret.AntialiasedLineEnable = false;
	return ret;
}

D3D11_BLEND_DESC GetDefaultBlendDesc() {
	D3D11_BLEND_DESC ret = {};
	ret.AlphaToCoverageEnable = false;
	ret.IndependentBlendEnable = false;
	ret.RenderTarget[0].BlendEnable = false;
	ret.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
	ret.RenderTarget[0].DestBlend = D3D11_BLEND_ZERO;
	ret.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	ret.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	ret.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	ret.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	ret.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	return ret;
}

D3D11_SAMPLER_DESC GetDefaultSamplerDesc() {
	D3D11_SAMPLER_DESC ret;
	ret.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	ret.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	ret.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	ret.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	ret.MinLOD = -FLT_MAX;
	ret.MaxLOD = FLT_MAX;
	ret.MipLODBias = 0.0f;
	ret.MaxAnisotropy = 1;
	ret.ComparisonFunc = D3D11_COMPARISON_NEVER;
	ret.BorderColor[0] = 1.0f;
	ret.BorderColor[1] = 1.0f;
	ret.BorderColor[2] = 1.0f;
	ret.BorderColor[3] = 1.0f;
	return ret;
}