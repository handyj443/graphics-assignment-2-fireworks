#include "Particles.h"

#include <vector>
#include <d3d11.h>
#include <DirectXMath.h>
#include <random>

using namespace std;
using namespace DirectX;

Particles::Particles(size_t n) : pos(n), vel(n), color(n), size(n), life(n),
	lifetime(n), type(n), valid(n), numParticles(0), MAX_PARTICLES(n), 
	newParticleIndex(0), oldestValidParticle(0) {
	pos.resize(n);
	vel.resize(n);
	color.resize(n);
	size.resize(n);
	life.resize(n);
	lifetime.resize(n);
	type.resize(n);
	valid.resize(n);
}

inline void Particles::AddSingleParticle(const XMFLOAT3 &initialPos, 
								  const XMFLOAT3 &initialVel, 
								  const XMFLOAT4 &initialColor, 
								  float initialSize, Type t) {
	if (numParticles < MAX_PARTICLES) {
		uniform_real_distribution<float> lifetimeVariance(0.60f, 1.30f);
		uniform_real_distribution<float> sizeVariance(0.50f, 1.40f);
		pos[newParticleIndex] = initialPos;
		vel[newParticleIndex] = initialVel;
		color[newParticleIndex] = initialColor;
		life[newParticleIndex] = 0.0f;
		switch (t) {
			case RISER:
			case HOLLOW_RISER:
				size[newParticleIndex] = initialSize;
				// precise timing is important for RISERs
				lifetime[newParticleIndex] = 
					RISER_EXPIRY_TIME;
				break;
			case ROCKET:
				size[newParticleIndex] = initialSize;
				lifetime[newParticleIndex] =
					ROCKET_EXPIRY_TIME * lifetimeVariance(e);
				break;
			case EXPLOSION:
				size[newParticleIndex] = initialSize * sizeVariance(e);
				lifetime[newParticleIndex] = 
					EXPLOSION_EXPIRY_TIME * lifetimeVariance(e);
				break;
			case TRAIL:
				size[newParticleIndex] = initialSize * sizeVariance(e);
				lifetime[newParticleIndex] =
					TRAIL_EXPIRY_TIME * lifetimeVariance(e);
				break;
		}
		type[newParticleIndex] = t;
		valid[newParticleIndex] = true;
		++numParticles;
		newParticleIndex = (newParticleIndex + 1) % MAX_PARTICLES;
	}
}

void Particles::AddFirework(const Firework &f) {
	switch (f.type) {
		// A riser is a single particle that rises to a certain height.
		case Firework::RISER:
			AddSingleParticle(f.launchPos, f.launchVel, f.color, 2.0f,
							  RISER);
			break;
			// A hollow riser is like a riser, but explodes as a hollow shell.
		case Firework::HOLLOW_RISER:
			AddSingleParticle(f.launchPos, f.launchVel, f.color, 1.0f,
							  HOLLOW_RISER);
			break;
			// A rocket rises but does not explode
		case Firework::ROCKET:
			AddSingleParticle(f.launchPos, f.launchVel, f.color, 4.0f,
							  ROCKET);
			break;
	}
}

// Generate an explosion of particles in all directions.
void Particles::AddExplosion(const XMFLOAT3 &initialPos, const float initialSpeed,
							 const XMFLOAT4 &initialColor, const float initialSize,
							 const float speedDeviation = 1.8f, 
							 const unsigned int explosionParticles = EXPLOSION_NUM_PARTICLES) {
	// Randomise velocity
	std::uniform_real_distribution<float> u(-1, 1);
	// speedDeviation controls the range of speeds;
	std::uniform_real_distribution<float> s(initialSpeed/speedDeviation, initialSpeed);
	XMFLOAT3 vel;
	for (unsigned int i = 0; i < explosionParticles; ++i) {
		vel.x = u(e);
		vel.y = u(e);
		vel.z = u(e);
		XMVECTOR velV = XMLoadFloat3(&vel);
		velV = XMVector3Normalize(velV);
		float speed = s(e);
		velV = XMVectorScale(velV, speed);
		XMStoreFloat3(&vel, velV);
		AddSingleParticle(initialPos, vel, initialColor, initialSize, EXPLOSION);
	}
}


void Particles::Clean() {
	// save range of buffer to iterate through, since we are modifying 
	// oldestValidParticle and numParticles
	size_t from = oldestValidParticle;
	size_t length = numParticles;
	for (size_t i = 0; i < length; ++i) {
		size_t ind = (i + from) % MAX_PARTICLES;
		if (!valid[ind]) {
			oldestValidParticle = (oldestValidParticle + 1) % MAX_PARTICLES;
			--numParticles;
		}
		else {
			break;
		}
	}
}

void Particles::Update(const float dt, const XMFLOAT3 &acc) {
	XMVECTOR posV, velV, accV;
	float inverseFadeDuration = 1 / FADE_DURATION;
	for (size_t i = 0; i < numParticles; ++i) {
		size_t ind = (i + oldestValidParticle) % MAX_PARTICLES;
		if (valid[ind]) {
			// Check if particle has lived past its lifetime
			switch (type[ind]) {
				case TRAIL:
					if (life[ind] > lifetime[ind]) {
						valid[ind] = false;
						color[ind].w = 0;
						continue;
					}
					break;

				case EXPLOSION:
					// Also, if particle lands on ground, it dies
					if (life[ind] > lifetime[ind] || pos[ind].y < 2.0f) {
						valid[ind] = false;  // flag for cleaning
						color[ind].w = 0;  // set alpha to zero
						continue;
					}
					else {
						// Add trail
						AddSingleParticle(pos[ind], { 0, 0, 0 }, color[ind],
										  TRAIL_QUAD_SIZE, TRAIL);
					}
					break;

				case RISER:
					if (life[ind] > lifetime[ind]) {
						AddExplosion(pos[ind], EXPLOSION_SPEED, color[ind],
									 EXPLOSION_QUAD_SIZE);
						valid[ind] = false;  // flag for cleaning
						color[ind].w = 0;  // set alpha to zero
						continue;
					}
					else {
						// Add trail
						AddSingleParticle(pos[ind], { 0, 0, 0 }, color[ind], 
										  TRAIL_QUAD_SIZE, TRAIL);
					}
					break;

				case ROCKET:
					if (life[ind] > lifetime[ind]) {
						valid[ind] = false;  // flag for cleaning
						color[ind].w = 0;  // set alpha to zero
						continue;
					}
					else {
						// Add trail
						AddSingleParticle(pos[ind], { 0, 0, 0 }, color[ind],
										  TRAIL_QUAD_SIZE * 2, TRAIL);
					}
					break;

				case HOLLOW_RISER:
					if (life[ind] > lifetime[ind]) {
						AddExplosion(pos[ind], HOLLOW_EXPLOSION_SPEED, color[ind],
									 EXPLOSION_QUAD_SIZE, 1.1f, 
									 EXPLOSION_NUM_PARTICLES / 2);
						valid[ind] = false;  // flag for cleaning
						color[ind].w = 0;  // set alpha to zero
						continue;
					}
					else {
						// Add trail
						AddSingleParticle(pos[ind], { 0, 0, 0 }, color[ind],
										  TRAIL_QUAD_SIZE, TRAIL);
					}
					break;
			}

			life[ind] += dt;
			posV = XMLoadFloat3(&pos[ind]);
			velV = XMLoadFloat3(&vel[ind]);
			accV = XMLoadFloat3(&acc);

			velV += XMVectorScale(accV, dt);
			// Linear drag model :
			// https://en.wikipedia.org/wiki/Drag_(physics)#Very_low_Reynolds_numbers:_Stokes.27_drag
			velV = XMVectorScale(velV, 1.0f - dragCoeff*dt);
			posV += XMVectorScale(velV, dt);

			XMStoreFloat3(&pos[ind], posV);
			XMStoreFloat3(&vel[ind], velV);

			// Fade out expiring particles gracefully
			if (life[ind] + FADE_DURATION > lifetime[ind]) {
				// For speed, multiply by inverse instead of doing a float divide 
				color[ind].w = (lifetime[ind] - life[ind]) * inverseFadeDuration;
			}
		}
	}
	Clean();
}